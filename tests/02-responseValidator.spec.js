// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/config/responseValidator');

// Local Variables
const response = {
    status: 'Access Denied',
    message: 'You are not authorized to access this resource'
};

// Tests
describe('Response Validator', () => {

    describe('exceptional cases', () => {
        it('should return null if passed null', () => {
            expect(validator(null)).to.be.null;
        });

        it('should return null if passed undefined', () => {
            expect(validator(undefined)).to.be.undefined;
        });

        it('should throw error when response is not a valid object literal', () => {
            expect(() => validator("Not an object literal")).to.throw();
        });

        it('should throw error when status property is not a String', () => {
            let clone = JSON.parse(JSON.stringify(response));
            clone.status = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when message property is not a String', () => {
            let clone = JSON.parse(JSON.stringify(response));
            clone.message = 1;

            expect(() => validator(clone)).to.throw();
        });
    });

    describe('success cases', () => {
        it('should return a valid response object when given a valid response object', () => {
            const responseData = validator(response);
            expect(responseData).to.be.an('object');
            expect(responseData).to.equal(response);
        });
    });
});