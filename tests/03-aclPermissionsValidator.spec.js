// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/acl/aclPermissionsValidator');

// Local Variables
const permission = {
    "resource": "users",
    "action": "allow",
    "methods": [
        "POST",
        "GET",
        "PUT"
    ]
};

// Tests
describe('ACL Permission Validator', () => {

    describe('exceptional cases', () => {

        it('should throw error when permission is not a valid object literal', () => {
            expect(() => validator("Not an object literal")).to.throw();
        });

        it('should throw error when resource property is missing', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            delete clone.resource;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when resource property is not a String', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.resource = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when action property is missing', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            delete clone.action;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when action property is not a String', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.action = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when methods property is missing', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            delete clone.methods;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when methods property is not an Array or a string', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.methods = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when methods property has no elements', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.methods = [];

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when methods property elements are not Strings', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.methods = [
                1,
                []
            ];

            expect(() => validator(clone)).to.throw();
        });
    });

    describe('success cases', () => {
        it('should return a valid permission object when given a valid policy object', () => {
            const aclData = validator(permission);
            expect(aclData).to.be.an('object');
            expect(aclData).to.equal(permission);
        });

        it('should accept a String for the Methods property', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.methods = "*";

            expect(clone).to.be.an('object');
            expect(clone).to.equal(clone);
        });

        it('should accept an Array for the Methods property', () => {

            let clone = JSON.parse(JSON.stringify(permission));
            clone.methods = [];

            expect(clone).to.be.an('object');
            expect(clone).to.equal(clone);
        });
    });
});
