// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/auth/routeValidator');

// Local Variables
const route = {
    name: 'putuser',
    method: 'PUT',
    path: '/user',
    spec: {
        method: 'PUT',
        path: '/user',
        version: '1.0.0'
    }
};

// Tests
describe('Route Validator', () => {

    describe('exceptional cases', () => {
        it('should when the route property of the request is null', () => {
            expect(() => validator(null)).to.throw();
        });

        it('should when the route property of the request is undefined', () => {
            expect(() => validator(undefined)).to.throw();
        });

        it('should when the route property of the request does not have a method property', () => {

            let clone = JSON.parse(JSON.stringify(route));
            delete clone.method;

            expect(() => validator(clone)).to.throw();
        });

        it('should  when the method property of the route is null', () => {

            let clone = JSON.parse(JSON.stringify(route));
            clone.method = null;

            expect(() => validator(clone)).to.throw();
        });

        it('should  when the method property of the route is undefined', () => {

            let clone = JSON.parse(JSON.stringify(route));
            clone.method = undefined;

            expect(() => validator(clone)).to.throw();
        });

        it('should when the route property of the request does not have a path property', () => {

            let clone = JSON.parse(JSON.stringify(route));
            delete clone.path;

            expect(() => validator(clone)).to.throw();
        });

        it('should  when the path property of the route is null', () => {

            let clone = JSON.parse(JSON.stringify(route));
            clone.path = null;

            expect(() => validator(clone)).to.throw();
        });

        it('should  when the path property of the route is undefined', () => {

            let clone = JSON.parse(JSON.stringify(route));
            clone.path = undefined;

            expect(() => validator(clone)).to.throw();
        });

    });

    describe('success cases', () => {
        it('should return a valid route object when given a valid route object', () => {
            const aclData = validator(route);
            expect(aclData).to.be.an('object');
            expect(aclData).to.equal(route);
        });
    });
});