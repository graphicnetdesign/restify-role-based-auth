// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/config/configValidator');

// Local Variables
const config = {
    aclFilename:   'custom.json',
    aclPath:       './security',
    baseApiUrl:    '/api'
};

// Tests
describe('Config Validator', () => {

    describe('exceptional cases', () => {
        it('should return null if passed null', () => {
            expect(validator(null)).to.be.null;
        });

        it('should return null if passed undefined', () => {
            expect(validator(undefined)).to.be.undefined;
        });

        it('should throw error when config is not a valid object literal', () => {
            expect(() => validator("Not an object literal")).to.throw();
        });

        it('should throw error when aclFilename property is not a String', () => {
            let clone = JSON.parse(JSON.stringify(config));
            clone.aclFilename = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when aclPath property is not a String', () => {
            let clone = JSON.parse(JSON.stringify(config));
            clone.aclPath = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when baseApiUrl property is not a String', () => {
            let clone = JSON.parse(JSON.stringify(config));
            clone.baseApiUrl = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when config object does not have any of the expected properties', () => {
            expect(() => validator({})).to.throw();
        });
    });

    describe('success cases', () => {
        it('should return a valid config object when given a valid config object', () => {
            const configData = validator(config);
            expect(configData).to.be.an('object');
            expect(configData).to.equal(config);
        });

        it('should return a valid config object if it has at least one of the expected properties', () => {
            let clone = JSON.parse(JSON.stringify(config));
            delete clone.aclFilename;
            delete clone.aclPath;

            expect(validator(clone)).to.have.any.keys('aclFilename', 'aclPath', 'baseApiUrl');
        });
    });
});