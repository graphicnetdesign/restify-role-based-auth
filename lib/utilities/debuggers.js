const debug = require('debug');
const app   = 'RRBA';

// Generic Logs
const errors = debug(`${app}:errors`);
const values = debug(`${app}:values`);

// Component-specific Logs
const authProc  = debug(`${app}:processors:authorizationProcessor`);
const index     = debug(`${app}:index`);

// Wrappers
function errorLogger(message){
    if(errors.enabled) errors(message);
}

function valuesLogger(message){
    if(values.enabled) values(message);
}

function authorizationProcessorLogger(message){
    if(authProc.enabled) authProc(message);
}

function indexLogger(message){
    if(index.enabled) index(message);
}

module.exports = {
    generic: {
        errorLogger,
        valuesLogger
    },
    component: {
        indexLogger,
        authorizationProcessorLogger
    }
};