// Node/NPM Modules

// Local variables
const utilities = require('../../utilities/utilities');

// Module functions
module.exports = function (route) {

    if(!route)                          throw new Error(`${utilities.moduleInfo.name}: The route property of the request is nonexistent.`);
    if(!route.hasOwnProperty('method')) throw new Error(`${utilities.moduleInfo.name}: The route does not have a method property`);
    if(!route.method)                   throw new Error(`${utilities.moduleInfo.name}: The method property of the route is nonexistent.`);
    if(!route.hasOwnProperty('path'))   throw new Error(`${utilities.moduleInfo.name}: The route does not have a path property`);
    if(!route.path)                     throw new Error(`${utilities.moduleInfo.name}: The path property of the route is nonexistent.`);

    return route;
};

// Private functions
