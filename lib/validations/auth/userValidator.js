// Node/NPM Modules

// Local variables

// Module functions
module.exports = function (user) {
    let roles = null;

    if(!user) return [];

    if(!user.hasOwnProperty('role')){
        if(!user.hasOwnProperty('roles')){
            return [];
        }
    }

    // Handle a user with a singular Role
    if(!user.role){
        if(!user.roles){
            return [];
        }
    }

    if(user.role && !(typeof user.role === "string")) return [];
    if(user.role) return [user.role];

    // Handle a user with an array of Roles
    if(user.roles && !(user.roles instanceof Array)) return [];
    if(user.roles && user.roles.length === 0) return [];
    if(user.roles) return user.roles;

    return roles;
};

// Private functions
