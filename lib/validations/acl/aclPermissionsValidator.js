// Node/NPM Modules

// Local variables
const utilities = require('../../utilities/utilities');

// Module functions
module.exports = function (aclPermission) {

    if(!utilities.isObjLiteral(aclPermission))          throw new Error(`${utilities.moduleInfo.name}: ACL Permission is not an object literal`);
    if(!aclPermission.hasOwnProperty('resource'))       throw new Error(`${utilities.moduleInfo.name}: ACL Permission does not contain the required resource property`);
    if(!(typeof aclPermission.resource === "string"))   throw new Error(`${utilities.moduleInfo.name}: ACL Permission resource property is not a String`);
    if(!aclPermission.hasOwnProperty('action'))         throw new Error(`${utilities.moduleInfo.name}: ACL Permission does not contain the required action property`);
    if(!(typeof aclPermission.action === "string"))     throw new Error(`${utilities.moduleInfo.name}: ACL Permission action property is not a String`);

    // Methods property can be either an array of methods
    // or a '*' wildcard symbol allowing any HTTP verb to be used
    if(!aclPermission.hasOwnProperty('methods'))        throw new Error(`${utilities.moduleInfo.name}: ACL Permission does not contain the required methods property`);
    if(!(aclPermission.methods instanceof Array) && !(typeof aclPermission.methods === "string"))   throw new Error(`${utilities.moduleInfo.name}: ACL Permission methods property is not an Array or a String`);
    if((aclPermission.methods instanceof Array) && aclPermission.methods.length === 0)              throw new Error(`${utilities.moduleInfo.name}: ACL Permission method property has no elements`);

    if((aclPermission.methods instanceof Array)) {
        aclPermission.methods.forEach((element) => {
            if (!(typeof element === "string")) throw new Error(`${utilities.moduleInfo.name}: ACL Permission method property elements are Strings`);
        });
    }

    return aclPermission;
};