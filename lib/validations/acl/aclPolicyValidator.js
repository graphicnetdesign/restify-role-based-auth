// Node/NPM Modules

// Local variables
const utilities = require('../../utilities/utilities');
const validator = require('./aclPermissionsValidator');

// Module functions
module.exports = function (aclPolicy) {

    if(!utilities.isObjLiteral(aclPolicy))          throw new Error(`${utilities.moduleInfo.name}: ACL Policy is not an object literal`);
    if(!aclPolicy.hasOwnProperty('role'))           throw new Error(`${utilities.moduleInfo.name}: ACL Policy does not contain the required role property`);
    if(!(typeof aclPolicy.role === "string"))       throw new Error(`${utilities.moduleInfo.name}: ACL Policy role is not a String`);
    if(!aclPolicy.hasOwnProperty('permissions'))    throw new Error(`${utilities.moduleInfo.name}: ACL Policy does not contain the required permissions property`);
    if(!(aclPolicy.permissions instanceof Array))   throw new Error(`${utilities.moduleInfo.name}: ACL Policy permissions is not an Array`);

    aclPolicy.permissions.forEach((element) => {
        validator(element);
    });

    return aclPolicy;
};