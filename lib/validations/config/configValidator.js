// Node/NPM Modules

// Local variables
const utilities = require('../../utilities/utilities');

// Module functions
module.exports = function (config) {

    // If null was passed in by user, it can be ignored
    // since config is not required.
    if(!config) return config;

    if(!utilities.isObjLiteral(config)) throw new Error(`${utilities.moduleInfo.name}: Config is not an object literal`);

    // Should have at least one of the expected properties
    if(!config.hasOwnProperty('aclFilename') &&
        !config.hasOwnProperty('aclPath') &&
        !config.hasOwnProperty('baseApiUrl')){
        throw new Error(`${utilities.moduleInfo.name}: Config does not have at least one of the expected properties {aclFilename, aclPath, baseApiUrl}`);
    }

    if(config.hasOwnProperty('aclFilename') && !(typeof config.aclFilename  === "string"))  throw new Error(`${utilities.moduleInfo.name}: Config role is not a String`);
    if(config.hasOwnProperty('aclPath')     && !(typeof config.aclPath      === "string"))  throw new Error(`${utilities.moduleInfo.name}: Config aclPath is not a String`);
    if(config.hasOwnProperty('baseApiUrl')  && !(typeof config.baseApiUrl   === "string"))  throw new Error(`${utilities.moduleInfo.name}: Config baseApiUrl is not a String`);

    return config;
};